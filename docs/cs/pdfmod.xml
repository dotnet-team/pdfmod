<?xml version="1.0" encoding="utf-8"?>
<?db.chunk.max_depth 1?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML 4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<book id="pdfmod" lang="cs">
	<bookinfo>
		<title>Příručka k aplikaci PDF Mod</title>
		<abstract role="description">
			<para>Toto je uživatelská příručka k PDF Mod, jednoduchému nástroji pro manipulaci s dokumenty PDF.</para>
		</abstract>
		<subtitle>Rychlá a snadná úprava PDF</subtitle>
		<copyright>
			<year>2009</year>
			<holder>Novell, Inc.</holder>
		</copyright><copyright><year>2009</year><holder>Marek Černocký (marek@manet.cz)</holder></copyright>
		<publisher>
			<publishername>Dokumentační projekt GNOME</publishername>
		</publisher>
		<authorgroup>
			<author role="maintainer">
				<firstname>Gabriel</firstname>
				<surname>Burt</surname>
				<affiliation>
					<orgname>Novell, Inc.</orgname>
				</affiliation>
			</author>
		</authorgroup>
		<revhistory>
			<revision>
				<revnumber>0.2</revnumber>
				<date>29.7.2008</date>
			</revision>
			<revision>
				<revnumber>0.1</revnumber>
				<date>25.7.2009</date>
			</revision>
		 </revhistory>
	</bookinfo>

	<chapter id="introduction">
		<title>Úvod</title>
			<para>PDF Mod je jednoduchý nástroj na provádění základních změn v dokumentech PDF.</para>

			<para>S tímto nástrojem můžete otáčet, odstraňovat a nebo extrahovat stránky v dokumentech PDF. Můžete také přidávat stránky z jiného dokumentu.</para>

			<para>Aplikaci PDF Mod můžete spustit ze své aplikační nabídky nebo spouštěče a nebo kliknutím pravým tlačítkem na jeden nebo více dokumentů PDF ve správci souborů a volbou Otevřít s PDF Mod.</para>

			<para>Podobně jako v jiných aplikacích určených k editaci, musíte změny, které v PDF Mod provedete, uložit. Pokud chcete původní dokument zachovat jak je a změny uložit do nového souboru, použijte Uložit jako.</para>
	</chapter>

	<chapter id="usage">
		<title>Použití</title>

		<sect1 id="opening-file">
			<title>Otevírání dokumentu</title>
			<para>Pokud chcete otevřít dokument v PDF Mod:</para>
			<itemizedlist>
			  <listitem>
				  <para>Zvolte <menuchoice><guimenu>Soubor</guimenu><guimenuitem>Otevřít</guimenuitem></menuchoice> a vyberte svůj dokument nebo</para>
			  </listitem>
			  <listitem>
				  <para>Zmáčkněte <keycombo><keycap>Ctrl</keycap><keycap>O</keycap></keycombo> a vyberte svůj dokument nebo</para>
			  </listitem>
			  <listitem>
				  <para>Přetáhněte dokument PDF ze své pracovní plochy nebo správce souborů do okna PDF Mod, ve kterém zatím není načtený žádný dokument, nebo</para>
			  </listitem>
			  <listitem>
				  <para>Zvolte dokument z nabídky <menuchoice><guimenu>Soubor</guimenu><guimenuitem>Nedávné dokumenty</guimenuitem></menuchoice>.</para>
			  </listitem>
		  </itemizedlist>
		</sect1>

		<sect1 id="zooming">
			<title>Přibližování</title>
			<para>Přibližováním můžete zvětšovat nebo zmenšovat náhledy stránek. PDF Mod se spouští s režimem Přizpůsobit, kdy se pokusí zobrazit stránky tak, aby byly vidět všechny naráz.</para>
			<para>Zvětšovat a zmenšovat můžete pomocí voleb v nabídce <menuchoice><guimenu>Zobrazit</guimenu></menuchoice> nebo zmáčknutím <keycap>Ctrl</keycap> a současným otáčením kolečka na myši.</para>
		</sect1>

		<sect1 id="properties">
			<title>Zobrazování a úprava vlastností</title>
			<para>Po otevření vlastností můžete zobrazit a upravit název, autora, klíčová slova a subjekt dokumentu. To provedete tak, že zvolíte <menuchoice><guimenu>Soubor</guimenu><guimenuitem>Vlastnosti</guimenuitem></menuchoice>, zmáčknete <keycombo><keycap>Alt</keycap><keycap>Enter</keycap></keycombo> nebo kliknete na tlačítko Vlastnosti na panelu nástrojů.</para>
		</sect1>

		<sect1 id="selection">
			<title>Vybírání stránek</title>
			<para>Aplikace PDF Mod umí automaticky vybrat všechny stránky, sudé stránky, liché stránky nebo stránky obsahující hledaný výraz. Tyto volby jsou k dispozici v nabídce <menuchoice><guimenu>Úpravy</guimenu></menuchoice>.</para>
			<para>Stránky můžete vybírat i ručně pomocí klávesnice nebo myši. V případě, že chcete vybrat více stránek, použijte <keycap>Ctrl</keycap> nebo <keycap>Shift</keycap>.</para>
		</sect1>

		<sect1 id="moving-pages">
			<title>Přesouvání stránek</title>
			<para>Pokud chcete přesunout (přeuspořádat) vybranou stránku nebo stránky, přetáhněte je v dokumentu na místo, kde je chcete mít.</para>
			<tip>
				<para>Všechny úpravy, vyjma odstraňování stránek, lze vrátit zpět volbou <menuchoice><guimenu>Upravit</guimenu><guimenuitem>Zpět</guimenuitem></menuchoice> nebo zmáčknutím <keycombo><keycap>Ctrl</keycap><keycap>Z</keycap></keycombo>.</para>
			</tip> 
		</sect1>

		<sect1 id="extracting-pages">
			<title>Extrahování stránek</title>
			<para>Extrahování vybrané stránky nebo stránek otevře nové okno PDF Mod s právě vybranými stránkami v novém dokumentu, který je připravený pro pozdější uložení.</para>
			<para>Pokud chcete extrahovat vybranou stránku nebo stránky, zvolte <menuchoice><guimenu>Upravit</guimenu><guimenuitem>Extrahovat stránku</guimenuitem></menuchoice>.</para>
			<tip>
				<para>Všechny funkce pro úpravy a výběr dostupné v nabídce <menuchoice><guimenu>Upravit</guimenu></menuchoice> jsou k dispozici i po kliknutí pravým tlačítkem na stránce. Některé funkce jsou dostupné i na panelu nástrojů.</para>
			</tip> 
		</sect1>

		<sect1 id="rotating-pages">
			<title>Otáčení stránek</title>
			<para>Pokud chcete otočit vybranou stránku nebo stránky, zvolte <menuchoice><guimenu>Upravit</guimenu><guimenuitem>Otočit stránku</guimenuitem></menuchoice> nebo zmáčkněte <keycombo><keycap>[</keycap></keycombo> pro otočení vlevo (proti směru hodinových ručiček) a <keycombo><keycap>]</keycap></keycombo> pro otočení vpravo (po směru hodinových ručiček).</para>
		</sect1>

		<sect1 id="removing-pages">
			<title>Odstraňování stránek</title>
			<para>Pokud chcete odstranit vybranou stránku nebo stránky, zmáčkněte <keycap>Delete</keycap> nebo zvolte <menuchoice><guimenu>Upravit</guimenu><guimenuitem>Odstranit stránku</guimenuitem></menuchoice>.</para>
			<warning>
				<para>V současnosti není možné tuto akci vrátit pomocí <menuchoice><guimenu>Upravit</guimenu><guimenuitem>Zpět</guimenuitem></menuchoice>. Místo toho můžete dokument zavřít bez ukládání a otevřít jej znovu se všemi stránky, čímž ale přijdete o ostatní změny, které jste v dokumentu provedli.</para>
			</warning> 
		</sect1>

		<sect1 id="saving">
			<title>Ukládání</title>
			<para>Po provedení změn v dokumentu máte dvě možnosti, jak svoji práci uložit. Můžete přepsat původní dokument volbou <menuchoice><guimenu>Soubor</guimenu><guimenuitem>Uložit</guimenuitem></menuchoice>, nebo můžete své změny uložit do nového souboru volbou <menuchoice><guimenu>Soubor</guimenu><guimenuitem>Uložit jako</guimenuitem></menuchoice>.</para>
		</sect1>

	</chapter>
	
</book>
