<?xml version="1.0" encoding="utf-8"?>
<?db.chunk.max_depth 1?>
<!DOCTYPE book PUBLIC "-//OASIS//DTD DocBook XML 4.1.2//EN" "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<book id="pdfmod" lang="pl">
	<bookinfo>
		<title>Instrukcja obsługi PDF Mod</title>
		<abstract role="description">
			<para>To jest instrukcja obsługi PDF Mod, prostego narzędzia do manipulacji dokumentami PDF.</para>
		</abstract>
		<subtitle>Szybka i łatwa edycja dokumentów PDF</subtitle>
		<copyright>
			<year>2009</year>
			<holder>Novell, Inc.</holder>
		</copyright><copyright><year>2009.</year><holder>Michał Sawicz (michal@sawicz.net)</holder></copyright>
		<publisher>
			<publishername>Projekt Dokumentacji GNOME</publishername>
		</publisher>
		<authorgroup>
			<author role="maintainer">
				<firstname>Gabriel</firstname>
				<surname>Burt</surname>
				<affiliation>
					<orgname>Novell, Inc.</orgname>
				</affiliation>
			</author>
		</authorgroup>
		<revhistory>
			<revision>
				<revnumber>0.2</revnumber>
				<date>2009-07-29</date>
			</revision>
			<revision>
				<revnumber>0.1</revnumber>
				<date>2009-07-25</date>
			</revision>
		 </revhistory>
	</bookinfo>

	<chapter id="introduction">
		<title>Wstęp</title>
			<para>PDF Mod to proste narzędzie pozwalające na podstawową edycję dokumentów PDF.</para>

			<para>Dzięki niemu możesz obracać, usuwać lub zgrywać strony dokumentu PDF. Możesz także dodać strony z innego dokumentu.</para>

			<para>PDF Mod możesz uruchomić poprzez menu aplikacji lub poprzez kliknięcie lewego przycisku myszy na jednym bądź kilku dokumentach PDF w przeglądarce plików i wybór otwarcia przy użyciu PDF Mod.</para>

			<para>Jak w przypadku każdego edytora, musisz zapisać zmiany wykonane w dokumencie. Jeśli chcesz zachować oryginalny dokument i zapisać zmiany, użyj opcji Zapisz jako.</para>
	</chapter>

	<chapter id="usage">
		<title>Sposób użycia</title>

		<sect1 id="opening-file">
			<title>Otwieranie dokumentu</title>
			<para>Aby otworzyć dokument w PDF Mod:</para>
			<itemizedlist>
			  <listitem>
				  <para>Wybierz <menuchoice><guimenu>Plik</guimenu> <guimenuitem>Otwórz</guimenuitem></menuchoice> i wybierz dokument lub</para>
			  </listitem>
			  <listitem>
				  <para>Naciśnij <keycombo><keycap>Ctrl</keycap><keycap>O</keycap></keycombo> i wybierz dokument lub</para>
			  </listitem>
			  <listitem>
				  <para>Przeciągnij dokument PDF z Pulpitu lub przeglądarki plików na okno PDF Mod w którym nie otwarto jeszcze dokumentu lub</para>
			  </listitem>
			  <listitem>
				  <para>Wybierz dokument z <menuchoice><guimenu>Plik</guimenu><guimenuitem>Ostatnio otwarte pliki</guimenuitem></menuchoice>.</para>
			  </listitem>
		  </itemizedlist>
		</sect1>

		<sect1 id="zooming">
			<title>Powiększenie</title>
			<para>Powiększenie zwiększy lub zmniejszy miniaturki stron. PDF Mod uruchamia się w trybie Najlepsze dopasowanie, w którym wszystkie strony są widoczne w tym samym czasie.</para>
			<para>Możesz powiększać lub powiększać używając opcji w menu <menuchoice><guimenu>View</guimenu></menuchoice> lub przytrzymując <keycap>Ctrl</keycap> i obracając kółko myszy.</para>
		</sect1>

		<sect1 id="properties">
			<title>Podgląd i edycja właściwości</title>
			<para>PDF Mod może automatycznie zaznaczyć wszystkie strony, strony parzyste lub nieparzyste albo strony zawierające dane wyrażenie. Opcje te dostępne są w menu <menuchoice><guimenu>Edycja</guimenu></menuchoice>.</para>
		</sect1>

		<sect1 id="selection">
			<title>Zaznaczanie stron</title>
			<para>PDF Mod może automatycznie zaznaczyć wszystkie strony, strony parzyste lub nieparzyste albo strony zawierające dane wyrażenie. Opcje te dostępne są w menu <menuchoice><guimenu>Edycja</guimenu></menuchoice>.</para>
			<para>Możesz także ręcznie wybierać strony używając klawiatury lub myszy. Użyj <keycap>Ctrl</keycap> lub <keycap>Shift</keycap> aby wybrać więcej niż jedną stronę.</para>
		</sect1>

		<sect1 id="moving-pages">
			<title>Przenoszenie stron</title>
			<para>Aby przenieść (zmienić kolejność) zaznaczoną stronę lub strony, przeciągnij je w dokumencie na pozycję w której mają się znaleźć.</para>
			<tip>
				<para>Wszystkie operacje z wyjątkiem usuwania stron mogą być cofnięte poprzez wybór <menuchoice><guimenu>Edycja</guimenu><guimenuitem>Cofnij</guimenuitem></menuchoice> lub naciśnięcie <keycombo><keycap>Ctrl</keycap><keycap>Z</keycap></keycombo>.</para>
			</tip> 
		</sect1>

		<sect1 id="extracting-pages">
			<title>Zgrywanie stron</title>
			<para>Zgranie zaznaczonej strony lub stron otworzy nowe okno PDF Mod zawierające wybrane strony w nowym dokumencie, gotowym do dalszej edycji lub zapisania.</para>
			<para>Aby zgrać zazaczoną stronę lub strony, wubierz <menuchoice><guimenu>Edycja</guimenu><guimenuitem>Zgraj stronę</guimenuitem></menuchoice>.</para>
			<tip>
				<para>Wszystkie operacje edycji i zaznaczania dostępne w menu <menuchoice><guimenu>Edycja</guimenu></menuchoice> są także dostępne po kliknięciu prawym przyciskiem myszy na stronie. Część operacji jest też dostępna w pasku narzędzi.</para>
			</tip> 
		</sect1>

		<sect1 id="rotating-pages">
			<title>Obracanie stron</title>
			<para>Aby obrócić zaznaczoną stronę lub strony, wybierz <menuchoice><guimenu>Edycja</guimenu><guimenuitem>Obróć stronę</guimenuitem></menuchoice> lub naciśnij <keycombo><keycap>[</keycap></keycombo> aby obrócić w lewo (przeciwnie do wskazówek zegara) lub <keycombo><keycap>]</keycap></keycombo> aby obrócić w prawo (zgodnie ze wskazówkami zegara).</para>
		</sect1>

		<sect1 id="removing-pages">
			<title>Usuwanie stron</title>
			<para>Aby usunąć zaznaczoną stronę lub strony, naciśnij <keycap>Delete</keycap> lub wybierz <menuchoice><guimenu>Edycja</guimenu><guimenuitem>Usuń stronę</guimenuitem></menuchoice>.</para>
			<warning>
				<para>Obecnie nie jest możliwe cofnięcie tej operacji poprzez menu <menuchoice><guimenu>Edycja</guimenu><guimenuitem>Cofnij</guimenuitem></menuchoice>. Możesz zamknąć dokument bez zapisywania i otworzyć ponownie aby odzyskać stronę, utracisz jednak wszystkie inne zmiany dokonane w dokumencie.</para>
			</warning> 
		</sect1>

		<sect1 id="saving">
			<title>Zapisywanie</title>
			<para>Po dokonaniu zmian w dokumencie, istnieją dwie możliwości ich zapisania. Możesz nadpisać oryginalny dokument wybierając <menuchoice><guimenu>Plik</guimenu><guimenuitem>Zapisz</guimenuitem></menuchoice> lub zapisać zmiany w nowym pliku wybierając <menuchoice><guimenu>Plik</guimenu><guimenuitem>Zapisz jako</guimenuitem></menuchoice>.</para>
		</sect1>

	</chapter>
	
</book>
